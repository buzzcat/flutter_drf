# flutter_drf

Flutter with Django Rest Framework

https://gitlab.com/buzzcat/flutter_drf

This project is a starting point for a Flutter application with working Email/Password login, registration etc together with Django Rest Framework and django-rest-auth.

There are a few other nice features added and ready such as retry, shared preferences, secure storage, localization, scoped_model and flushbar error/info messages.

## Getting Started

You will need to setup a Django server. I highly recommend using:

https://github.com/pydanny/cookiecutter-django (you don't have to use this but it will save you a bunch of time!)

Then also add:

https://github.com/Tivix/django-rest-auth

and set your domain and paths in user_model.dart.

Alternatively you can checkout the Django template at https://gitlab.com/buzzcat/flutter_drf_django which should get you started in no time.

I make some assumptions to url setup in django-rest-auth:

```
path('auth/', include('rest_auth.urls')),
path('auth/registration/', include('rest_auth.registration.urls')),
```

For my own purposes (and also what I think is quite standard) I have removed the use of username and revert to email login only in django/djang-rest-auth. It should be fairly straightforward to add facebook, google login etc using django-allauth.
